# Harmful Things

## Languages

### Go

I'm going to copy this thing I posted on reddit a while back.

>I've been using go for ~8 months now working on a networking application at work, and I've used several other languages for other projects of varying scales for several years now.

>But I can't get over the feeling that I spend more time writing boilerplate code in Go than I would in most other languages. Like being able to do a deep copy of a struct with private members I have to manually write out a .Clone function for it. Or sorting arrays of a struct by some member of it (think account balances, or something like that), requires defining a new type and implementing 3 functions for it, plus some conversion logic at the call site. Or sifting through an array to find a particular element or mapping values in the array to another.

>There's also lots of just awkward-looking things that I can't get comfortable with in a language like Go.

```
if _, ok := someInterface.(*someType); !ok {
    // some code with someInterface
}
```

>Or the equivalent for checking membership in a map.

>Or array manipulation...

>someArray = append(someArray, someValue)

>I understand why this might make sense with a vector in C, but even in C it doesn't have to look like this, and we're using a much higher level language than C anyways.

>It also annoys me how crippled the reflection is so that a lot of things end up needing to be exported when they shouldn't be, just because I want to convert something to JSON with encoding/json. Plus the fact that capitalization decides export status, so if I promote a member I have to change everything in the module that touches it.

>And I'm not even going to go into a rant about the if err != nil { return nil, err } meme, I'm so tired of writing that out.

>Hell, both go get and now the new module tool seem like things that could have easily been written in an afternoon, it seems like such low effort.

>I'm aware that some of these problems are going to be addressed in coming Go releases, but so much code has already been written with the current design of the language.

>It just feels like there's so much overhead when working with Go that it's difficult for me to describe the concepts that would be extremely easy to in so many other languages. I have a hard time understanding why Go is so popular despite its obvious shortcomings.

## Technologies

### Windows

Just don't use it.

