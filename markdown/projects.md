# Major Projects

You really should just look at my GitLab account if you want to see interesting things.  I only put certain stuff here and there's more to see over there anyways.

### Roz / Fulgurite

*[GitLab](https://gitlab.com/delbonis/roz)*

When I was at the DCI I worked on [Lit](https://github.com/mit-dci/lit).  This
was a lightning implementation separate from those in the BOLT family, although
now it's working towards BOLT compatibility.  So Roz was supposed to be
reimplementing Lit in Rust, trying to be compatibile with its protocol

I ended up also using it as a repo for implementing "eXtensible channels", which
was the [Fulgurite](https://tr3y.io/media/docs/fulgurite.pdf) concept that I had
spent some time on.  It's really cool, and you should read that paper.

It's still in progress since there's some implementation details that I still
have to work through.  When it's finished I'll standardize it and make an BOLT
spec for it so other lightnings can implement it.  But for now it's still very
beta, but what does work works well.

### Satoshi's Game of Light / Conway's Place

*[GitLab](https://gitlab.com/treyzania/gameoflight)*

Basically it's [satoshis.place](https://satoshis.place) but it's
[Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

I worked on this at the 2018 Chaincode Lightning residency, to a prototype.
I fully intend to make a "real" deployment of it on mainnet, but that's for later.

I was recorded on the final day of the residency [here](https://www.youtube.com/watch?v=WHo1Uhedrgc).

### QuickPixel

*[GitLab](https://gitlab.com/treyzania/quickpixel)*

It's a Snapchat clone that uses IPFS for distributing user identify data, while
passing around message payloads e2e encrypted in a nice way that tries to
preserve the "once you view it it gets deleted" system with some fun logic
based on hash preimages.

Again, not finished yet, but the core cryptographic gadgets work and some basic
server functionality is completed.  Client apps aren't finished yet.

### Redstone Description Language (RedDL)

*[GitLab](https://gitlab.com/treyzania/reddl-lang)*

I had this cool idea for a language like an HDL but for describing Minecraft
redstone circuits.  It would have primitives for describing connections of
busses and components on those busses, and then have ways to calculate exactly
how long the signal propagation time is and be able to make optimizations based
on that.  It uses a constraint solver for determining actual bus layouts and
things.

I've put this to the side for a while, but there was some fun core stuff that I
had worked out so far.

# Minor Projects

### Vader

*[GitLab](https://gitlab.com/treyzania/vader)*

Vader was this project I worked on to replace Pipenv, but done in a way I like a
lot more than how Pipenv does it.  Dependencies are built and kept track of
version-aware, so you don't have to bother with virtualenvs at all.  The name
"**V** a **d** e **r**" comes from "venvs done right".

The core functionality works pretty well, but it's not ready for production use
really I don't think.

### Bootybot

*[GitLab](https://gitlab.com/treyzania/bootybot)*

This is a script I wrote for automaticaly ingesting movies and TV shows for my
Plex server.  You just give it the directory where to find a set of new files,
like a torrent, and then it figures out what to do based on what it finds there.

Some of the renaming functionality is from wrapping Filebot with some extra
logic and support for the special case overrides where needed.

### Subsonic

*[GitLab](https://gitlab.com/treyzania/subsonic)*

I spent some time designing a message-passing-oriented microkernel

I didn't get very far implementing it, but I did design a novel build system in
Python for it which is pretty cool.

### Jiyunet

*[GitLab](https://gitlab.com/jiyunet/jiyunet)*

Distributed messageboard on a blockchain.  Users are identified by their public
keys and submit messages signed with their private keys.  It's really more of a
generic system for "putting arbitrary data on a blockchain", so there's only
really limited uses of it since there's better ways of doing that.

**Plans:**

* Currently I haven't finished the networking code, but I'm planning on having
it be a layer on top of IPFS's networking code.

* I should change how block information is transmitted, as the way the signature
data is laid out makes parsing different versions in the future more
complicated.

### Neocore

*[GitLab](https://gitlab.com/treyzania/neocore)*

Minecraft server administration and management platform.  I haven't really been
working on this a lot because I don't play Minecraft as much as I wish I still
did and I have cooler problems to work on now anyways.  It's mostly from ~2016.

**Plans:**

* Have servers talk to each other via redis and use that for caching active
player data instead of `nmd`.

* Generalize the "service" system to allow service types to be registered
dynamically.

### Pew

*[GitLab](https://gitlab.com/treyzania/pew)*

Just a cute little Android game in space!  I built this in high school as a
side project to learn Unity and it was a lot of fun.  Maybe I'll rewrite it
eventually in Godot!
