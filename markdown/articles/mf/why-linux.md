# Why Linux? / Why free software?

<!-- tz-insert-toc -->

## Introduction

There's countless individual reasons why Windows sucks and nobody should use it, so I'm going to focus more on the ideological differences and how they give rise to practical differences for end users.  I'm also going to avoid addressing macOS here too much because (1) some of these arguments still apply, (2) the people that use macOS tend to want it because it's Apple, (3) switching wouldn't be very hard so they don't need much convincing.  There is a bit of nuance to it, but that's out of scope for this discussion.

The free software movement focuses on many aspects of software (actual price being of lesser importance) but a common theme between it all is respecting the personal autonomy of the user and the software not acting as if it always knows better than the user.  Another aspect involves treating the software more like knowledge to be shared and built upon instead of (intellectual) property that needs to be managed by a sole owner and where people are merely given the privilege of accessing it.

## Part 1 - History

It's important to characterize what Microsoft is like as a company.  I think it's important to analyze things in terms of incentive structures, and we can use past behavior to try to understand the incentives that actors are following.

One of the earliest examples of Microsoft being Microsoft was the DOS wars in the 80s.  Most people aren't aware that there was other DOSes than MS-DOS.  PC-DOS, DR-DOS, FreeDOS, and others.  For the most part, programs written for one DOS could run just fine another DOS.  Microsoft made early versions of Word and their other products print a warning message if it detected that they were running on any DOS other than MS-DOS, even though it ran just fine.  So this plus other warnings they gave to decision makers in companies made them very afraid to use DOSes other than MS-DOS, and Microsoft crushed the competition.

Microsoft, once being the most valuable corporation in the world, now has immense power over the entire technology industry.  They want to keep being a massive corporation and they have the means to make that happen.

The GNU project started in the 80s in the wake of RMS getting really angry at printer manufacturers for being bad, even worse than they are today.  Independently, the Linux project got started as a side project in 1991.  These projects grew out of academia and had no profit motive, and the earliest distributions were put together just by normal people in their free time.  This kind of project saw enthusiastic adoption among a somewhat technically-inclined crowd, and for a brief weird time (nearly 30 years ago!), some game publishers, in the interest of reaching as wide as an audience as possible, even made native Linux releases of their games.  Not random games people has never heard of, games like Quake.

But that got Microsoft *really* scared.  A free and open source operating system that respected the freedoms of its users?  Early Windows couldn't possibly compete in the long run.  So they changed the game.  They poured millions of dollars into marketing campaigns and other propaganda to slander the early FOSS movement.  You probably still may have heard the lies repeated today:

* "open source is insecure because anyone can contribute to it and insert bugs" (that's just not how that works)
* "open source is insecure because anyone can read the code and discover bugs in it" (security through obscurity fallacy)
* anything about software support (anybody that's making FOSS for businesses offers paid hands-on support)
* basically anything that sounds derogatory to free software

The reasons about security are especially ironic because of the atrocious track record that Windows has.  Windows security vulnerabilities are always *way* funnier than anything else because of the sheer scope of their fuckups.  That could also be a little schadenfreude.

They also took more direct efforts to establish Windows as *the* singular OS for businesses and to undermine alternatives:

* Incentivized OEMs to preinstall Windows on consumer devices, to ensure that people are already familiar with it.
* Sabotaging OpenGL, wasting industry effort on FireGL, then launching proprietary DirectX later.
* Giving free support for game developers to port their games over to DirectX.
* Only releasing Office on Windows, naturally.  MacOS releases came later when it was no longer a risk.
* Various kinds of vendor lock-in with enterprise Windows networks.
* All kinds of things to play ball with DRM developers, which is a discussion for another time.
* other things I'm probably forgetting

The internal corporate motto is "Embrace, Extend, and Extinguish".  This was especially a big deal during the Browser wars of the late 90s and early 2000s with Internet Explorer, ActiveX nonsense, later Silverlight vs Flash, Java, etc.  The Wikipedia article on that is good enough.  (As an aside, the entire existence of C# is to lock desktop application developers into the Microsoft .NET ecosystem.  That's why it's so similar to Java.  It's vomit inducing to think that someone would want to try to *own* a language that people use to express ideas.  Could you imagine if the French government tried to sue you for speaking French outside France?  Yet multiple companies tried to do that in the 90s up until around the mid-2000s.)

And more recently:

* Introducing UEFI as *the* firmware standard.
* Requiring Secure Boot be configured with *only* Microsoft's signing keys to allow devices to be officially certified to run Windows (which of course OEMs did).
* Defined the OpenDocument standard (.docx vs .doc, etc.), and then not actually following it so that documents from MS Office opened in LibreOffice are still broken.
* other things I'm probably forgetting
* literally the other day I found out: something really involved with hardware power states that I don't feel like explaining, but they decided to introducing unilaterally without going through the normal standards bodies, so it broke suspend support in Linux on new laptops (post 2020) and we are still working on supporting it properly, it's a symptom of a greater narrative.

After Satya Nadella came to the leadership, things did change somewhat.  Developers were leaving Windows in droves because all the good tooling was on Linux or macOS.  The old strategy of EEE didn't work anymore because people realized what was up.  It was too ingrained in Microsoft's corporate culture to remove, so they changed course.  People left Windows for Linux because of the dev tools, but they stayed because of the culture and ideology.  So what Microsoft did was introduce WSL.  It was a basic way for people to use Linux dev tools on Windows, in a Linux-ish environment.  From a technical perspective, it was pretty cool, but it also kinda sucked a lot of things didn't work.  But it worked just well enough that suddenly the perceived benefit of switching for most devs just wasn't there anymore.  They launched this long campaign of "Microsoft <3 Linux" in order to try to pave over their earlier war crimes, and now there's an entire generation of developers getting started that don't remember their earlier sins.  They became of corporate sponsor of the Linux Foundation, but their contributions are almost exclusively to make Linux work better in WSL and on Azure, their cloud infrastructure service.  If they really "loved" Linux, they'd be contributing to WINE, releasing Office for Linux, and taking an entirely different track.  They only "love" it to the extent that it deepens their position, and fuck anyone that thinks otherwise.

This also brings us to the GitHub acquisition.  GitHub spent a really long time building themselves as *the* place where open source development happens.  But because of corporate mismanagement (they went without a CEO for a few months, among other things), their investors were looking for an out.  Along comes Microsoft with infinitely deep pockets and decides to buy them out and rescue them from that.

I don't want to get into the Minecraft situation with "Windows 10 edition" and transitioning to Microsoft accounts, but you can probably fill in the blanks at this point.

To conclude this section, some of big ideas are these:

* Microsoft has incredible amounts of resources and strong incentives that are not aligned with yours.
* Computers are a fundamental part of society and culture now, and nobody should be able to unilaterally control how you interact with society to the degree they do.
    * I could go more into this in another writeup...

Update: I was made aware of the history that Microsoft had with Corel, the makers of WordPerfect which was a competitor to Microsoft Word for a very long time.  Corel saw the competition that Microsoft was and doubled down on investing in the Linux ecosystem in the mid/late 90s and even began their own Linux distribution, in order to be able to better sell productivity software and support into that ecosystem and make it a viable alternative in the office environment.  Microsoft [invested a bunch of money in Corel](https://news.microsoft.com/2000/10/02/corel-and-microsoft-announce-strategic-alliance/) and acquired shares, which naturally led to them dropping their interest in Linux soon after.  If they just got $135M of fresh funding then wouldn't they be happy to continue development on their Linux stuff?

## Part 2 - Success despite it all

Things just work.  The way software development in corporations works tends to be focused on delivering products, and bigger picture work to pay down technical debt and change things for the better tends to happen very rarely.  Software that people work on for the joy of the craft tends to be better.  The kinds of people that do it for the joy of the craft often have almost autistic obsessions with developing good software (including myself, unfortunately).

Since computers have integrated themselves so deeply into our society (and in a relatively short period of time, about 30 years), and they forms a new medium with which our culture plays out.  The tools that we use to interact with this new medium are extensions of ourselves and we shouldn't have to settle for using software developed by actors that don't have our best interests at heart.  It puts you in a position to be taken advantage of and subjugated, and what you get out of it isn't worth selling yourself into serfdom.

To speak more practically about it, there's plenty of reasons:

* In day to day life aren't many things that you just "can't do" for unexplained reasons.  If there seems like a way to do something, there probably is.  Things also tend to not break for unexplained reasons like they do on Windows.
* *Package managers!*  I don't need to explain this here.  There's better explanations elsewhere.
* On Windows, if you have two hard drives in your computer it's tricky and not fully supported to store your user data (`C:\Users\Me`) on another hard drive (like, `D:\Users\Me`).  You can do it, but you kinda have to do it as you're setting up a new install and not every program will be happy about it.  A few months ago I got a new NVMe drive and moved over my computer's `/home` directory onto it and when I booted back into the OS it didn't even know the difference.  It just werks.
* On Ubuntu, GNOME Online Accounts has support to integrate pretty much everything cloud service there is right into the system file manager, if that's something you're into (which you might be, starting off).
* Rarely have to worry about drivers.  I mention this below, but if a driver is in the main tree, you don't have to do *anything* to update it.  Nvidia sucks though and some wifi dongles require extra firmware, but that's not as much of an issue as it used to be.
* The ecosystem more compatible with more things.  For a long time (and still today really), unless you wanted to use FAT, there was no filesystem for portable storage devices that worked well on every OS.  MacOS liked HFS(+), Windows liked NTFS or FAT32.  Linux *likes* ext4, but there's support for pretty much every fs that exists.  Pretty much every webcam under the sun just werks out of the box, zero setup required.

I could go on for a while, but those are the bigger reasons.

These days there are some businesses made on providing for the free software community.  Most of their income is in enterprise support.  Some successful examples are:

* Canonical (despite their flaws)
* Mozilla (well, most of their funding is from Google trying to look like not a monopolist and they have other problems)
* RedHat (until they got bought by IBM, for some fucking reason)
* NGINX
* literally dozens of others, probably more

There *is* an element of street cred to it.  But it's not really seen as something to brag about.  Use a Thinkpad running QubesOS if you want to brag.  Historically, it was a right of passage to use it as your daily driver and it's a signal that you're serious about what you're doing with your personal computing.  But because Microsoft (and others) is starting to sabotage the culture and corporations control the narrative of popular software development, this view is harder to come by.  Now everybody is a code artisan using Macbooks sitting in Starbucks writing ~~Ruby on Rails~~Node.js on top of MongoDB in ~~Sublime Text~~VS Code.  Oh yeah that's another one, everybody uses VS Code these days too.

### On "muh gaems"

(Context: WINE is a project for running Windows software on Linux, it works really well.  Proton is a extension of it from Valve built into Steam that's customized for running games.)

I don't know the history of WINE very well, but it's a great project.  Gabe Newell started his career at Microsoft before starting Valve, and while he doesn't speak out about it very often, it's pretty clear he rejects their ideology.  Valve is not morally pure, they've done some problematic things in the past, but as far as respecting the freedoms and autonomy of its users they're better than pretty much every other game developer.  (And of course they don't release their games as free software, but that's not really as much of a problem for games as it is for other categories of software.)  A couple of years ago, Valve started funding some development for WINE in response to the Windows 8 and UWP debacle, as Gabe saw the writing on the wall that Microsoft wants to control the PC gaming industry with even more of a stranglehold than they do already.  There was also that guy that really wanted to play Nier: Automata on Linux so he started working on DXVK, which was an implementation of DirectX 11 on top of Vulkan, and it lets *countless* games just werk that wouldn't otherwise.  Not random Unity games (which do work well), AAA games like GTA, Fallout, The Witcher III, Sekiro, Hitman 2, Cyberpunk 2077, etc.  And even VR games like Beat Saber work really well.  (That one time I didn't get Beat Saber working is because there was an update and I forgot to do a thing that I should have done, and I was also trying to have a nice night drinking with friends.  VR is still problematic sometimes in Windows, even when you're staying within the lines of what the corporate overlords have decided you're allowed to do.  It worked before then and it's worked ever since.)

Check out protondb.com to check how different games are doing.  In the top 100 games on Steam, 80% have Gold, Platinum, or Native ratings.  Also check out Lutris, sometimes there's some quirks that haven't made it into Proton that Lutris can work with if someone's made a configuration for it.

## Part 3 - Reasons

People have a lot of excuses as to why they can't use Linux.  Some of them are actually valid (even if it's unfortunate), but there's a lot that are just shortsighted.  So I'm going to refute a few of the more commonly cited ones.

### Bad reasons:

* I can't use some particular weird software on Linux.
	* WINE probably works as long it's not 3D accelerateed.
	* Check if there's alternatives.
* I can't play this game on Linux, and it doesn't work in WINE.
	* If the issue is because of some weird DirectX thing, then that's probably a known issue with people working on it.
	* If the issue is because of DRM, look into using a pirated version that has the DRM stripped (see below).
	* If the issue is because of anti-cheat, and if there's still cheaters in the games then it's because the publisher wants to feel powerful (see below), consider supporting a company that's less shitty.
		* There's some effort to make EasyAntiCheat work in WINE, but it's not ready yet.
* I'd have to learn how it works all over again.
	* Microsoft changes how Windows looks every couple of years, Google changes how Android looks every couple of years, even websites change how they look every couple of *months*.
		* And when they change things it's usually in service of them (user engagement, etc.), if it helps the user be better that's an unintended side effect.
	* Learning things is good, and you'll be better because of it.
* I'm developing software for Windows.
	* why???
	* It's also possible to write Windows software on Linux, use Vagrant or something if you have to.
* I'm not a technical person and don't know how to use a terminal.
	* My dad is a 64 y/o hydraulic engineer and has been using Windows for as long as he's owned a computer, and he started using Ubuntu as his daily driver personal computer about a year and a half ago.  He never has to open a terminal.
	* On the off chance you might have to, you can ask a friend to help.
		* In fairness, you might need a friend for doing the initial install.  In a perfect world Canonical would have the resources to be able to handle the quirks in every random piece of hardware.
	* Learning things is still not a bad thing.
* I have an Nvidia graphics card and the drivers suck.
	* Nvidia is in bed with Microsoft more than most and they don't actually care about Linux beyond supporting it for AI training and things in server settings.
	* The drivers suck less than they used to, but that was when I already decided that I'm never buying an Nvidia product again.
	* Buy an AMD graphics card.  You plug it in and it just werks.  No manual driver installation needed.
* I want to use Photoshop. (not for work)
	* Learn something new and use a different tool, like Krita, Inkscape, kdenlive, etc.
	* Look at this: https://github.com/Gictorbit/photoshopCClinux
		* Adobe does some funky stuff that breaks WINE sometimes, but there's nothing like DRM actually fundamentally stopping it.  Adobe benefits from piracy when individuals do it, think about where they make the most money.  It's the same reasons that Microsoft wanted as many consumers as possible to use Windows.
* I want to use Maya/3DS Max/something. (not for work)
	* Just use Blender.
	* See below comment on Adobe about petitioning the developer.

And everything here should be taken with a massive massive grain of salt: **You can have it both ways.**  Dual booting is something that personal computers have been able to do almost since the dawn of time.

From above: DRM is fundamentally a flawed concept because of the analog loophole (see Wikipedia).  Anti-cheat is really misused.  The popular one to talk about is Vanguard because of how invasive it is, but practically speaking game publishers use it to allow themselves to hire cheaper developers instead of hiring people that know (or spending money to train people to learn) how to write game netcode that's harder to cheat with.  Minecraft servers have better free and open source community-developed non-invasive server-side only anti-cheat systems than AAA games like Valorant.  I could probably do an entire writeup on these ideas, too, but it'd just be reiterating a lot of the points I already made here.

### Good reasons:

* I *need* to use Photoshop (or some 3D modeling software, or CAD software) for work, and that link above doesn't satisfy me for some reason.
	* This sucks, but Adobe doesn't have any particular investment in Windows (certainly not more than they do macOS), so if enough people ask for Linux releases then they could get on board.
	* You're probably running a Mac anyways if this is true for you.
* Corporate policy something something
	* Also sucks
	* Maybe complain to HR.

Maybe some others.  I'd have to think about it.

## Conclusion

So that's a brief overview of it all.  If I'm wrong about any of the particulars of the historical parts then let me know and I'll correct them.

my hands are getting tired

