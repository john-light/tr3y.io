#!/usr/bin/env python3

import os
import subprocess
import sys
import shutil
import pathlib

output = sys.argv[1]

print('exporting to \'' + output + '\'')

# Make sure the directory doesn't already exist.
if not os.path.exists(output):
    os.mkdir(output)
else:
    print('directory already exists!')
    sys.exit(1)

# Copy the static files
for dp, dns, fns in os.walk('static'):
    for fn in fns:
        if fn.endswith('~'):
            continue
        shutil.copy2(os.path.join(dp, fn), os.path.join(output, fn))

def render_toc(path):
    cmd = 'pandoc --from gfm --template pandoc-only-toc.template --toc --standalone --metadata title=- %s' % path
    print(cmd)
    p = subprocess.Popen(cmd.split(), shell=False, stdout=subprocess.PIPE)
    p.wait()
    out, _ = p.communicate()
    return str(out, 'utf8')

# Convert and export the markdown files.
def export_markdown(path):
    infile = os.path.join('markdown', path)

    render_buf = []
    title = None
    with open(infile, 'r') as f:
        for i, l in enumerate(f.readlines()):
            if 'tz-insert-toc' in l:
                render_buf.append(render_toc(infile))
            else:
                render_buf.append(l)
            if l.startswith('# ') and title is None:
                title = l[2:]

    render_buf = ''.join(render_buf)

    outfile = os.path.splitext(os.path.join(output, path))[0] + '.html'
    os.makedirs(os.path.dirname(outfile), exist_ok=True)

    cmd = [
        'pandoc',
        '--from', 'markdown',
        '--template', 'pandoc-article.template',
        '--standalone',
        '--include-in-header=mdheader.htm',
    ]

    cmd.append('--metadata')
    if title is not None:
        cmd.append('title=%s :: tr3y.io' % title.strip())
    else:
        name = os.path.splitext(os.path.basename(path))[0]
        cmd.append('title=%s :: tr3y.io' % name)

    # Add on the output and inputs.
    cmd.extend(['-o', outfile, '-'])

    print(' '.join(cmd))
    p = subprocess.Popen(cmd, shell=False, stdin=subprocess.PIPE)
    p.communicate(input=render_buf.encode())

for dp, dns, fns in os.walk('markdown'):
    for fn in fns:
        if fn.endswith('~'):
            continue
        export_markdown(os.path.relpath(os.path.join(dp, fn), 'markdown'))

